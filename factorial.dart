import 'dart:io';

int calcularFactorial(int n) {
    // Caso base: el factorial de 0 es 1
    if (n == 0) {
        return 1;
    }

    // Caso recursivo: n! = n * (n-1)!
    return n * calcularFactorial(n - 1);
}

void main() {
    stdout.write('Ingrese un número: ');
    int numero = int.parse(stdin.readLineSync()!);

    int factorial = calcularFactorial(numero);
    print('El factorial de $numero es $factorial');
}