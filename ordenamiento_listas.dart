void ordenamientoBurbuja(List<int> lista) {
    int n = lista.length;

    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (lista[j] > lista[j + 1]) {
                // Intercambiar lista[j] y lista[j+1]
                int temp = lista[j];
                lista[j] = lista[j + 1];
                lista[j + 1] = temp;
            }
        }
    }
}

void main() {
    // se podria promptear para que el usuario llene esto
    List<int> numeros = [5, 2, 8, 1, 9, 3, 40, 0, 99];
    print("Lista original: $numeros");

    ordenamientoBurbuja(numeros);

    print("Lista ordenada: $numeros");
}