import 'dart:io';

double calcularDescuento(double precio, double porcentajeDescuento) {
    double descuento = precio * (porcentajeDescuento / 100);
    double precioFinal = precio - descuento;
    return precioFinal;
}

void main() {
    print("Calculadora de Descuentos");

    stdout.write("Ingrese el precio original: ");
    double precio = double.parse(stdin.readLineSync()!);

    stdout.write("Ingrese el porcentaje de descuento: ");
    double porcentajeDescuento = double.parse(stdin.readLineSync()!);

    double precioFinal = calcularDescuento(precio, porcentajeDescuento);

    print("El precio original era \$${precio.toStringAsFixed(2)}");
    print("Con un descuento del ${porcentajeDescuento.toStringAsFixed(2)}%");
    print("El precio final es \$${precioFinal.toStringAsFixed(2)}");
}