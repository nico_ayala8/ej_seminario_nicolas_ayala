import "dart:io";

int contarOcurrencias(String texto, String palabraBuscar) {
    int conteo = 0;
    int indice = 0;

    while (indice < texto.length) {
        indice = texto.indexOf(palabraBuscar, indice);

        if (indice == -1) {
            // no se encontro
            break;
        }

        conteo++;
        indice += palabraBuscar.length;
    }

    return conteo;
}

void main() {
    print("Buscador de palabras en un texto");

    stdout.write("Ingrese el texto: ");
    String texto = stdin.readLineSync()!;

    stdout.write("Ingrese la palabra a buscar: ");
    String palabraBuscar = stdin.readLineSync()!;

    int conteo = contarOcurrencias(texto, palabraBuscar);

    print("La palabra '$palabraBuscar' se encontró $conteo veces en el texto.");
}