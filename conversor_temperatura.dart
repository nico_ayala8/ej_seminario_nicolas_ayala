import 'dart:io';

double celsiusToFahrenheit(double celsius) {
    double fahrenheit = (celsius * 9 / 5) + 32;
    return fahrenheit;
}

double fahrenheitToCelsius(double fahrenheit) {
    double celsius = (fahrenheit - 32) * 5 / 9;
    return celsius;
}

void main() {
    print("Conversor de temperatura");
    print("1. Celsius a Fahrenheit");
    print("2. Fahrenheit a Celsius");

    stdout.write("Ingrese una opción: ");
    int opcion = int.parse(stdin.readLineSync()!);

    if (opcion == 1) {
    stdout.write("Ingrese la temperatura en Celsius: ");
    double celsius = double.parse(stdin.readLineSync()!);
    double fahrenheit = celsiusToFahrenheit(celsius);
    print("$celsius°C = $fahrenheit°F");
    } else if (opcion == 2) {
    stdout.write("Ingrese la temperatura en Fahrenheit: ");
    double fahrenheit = double.parse(stdin.readLineSync()!);
    double celsius = fahrenheitToCelsius(fahrenheit);
    print("$fahrenheit°F = $celsius°C");
    } else {
    print("Opción inválida");
    }
}