import 'dart:io';

List<String> tareasPendientes = [];
List<String> tareasCompletadas = [];

void mostrarMenu() {
    print('===== To-Do List =====');
    print('1. Agregar tarea');
    print('2. Marcar tarea como completada');
    print('3. Eliminar tarea');
    print('4. Ver tareas pendientes');
    print('5. Ver tareas completadas');
    print('6. Salir');
    stdout.write('Ingrese una opción: ');
}

void agregarTarea() {
    stdout.write('Ingrese la tarea: ');
    String nuevaTarea = stdin.readLineSync()!;
    tareasPendientes.add(nuevaTarea);
    print('Tarea agregada correctamente.');
}

void marcarTareaCompletada() {
    if (tareasPendientes.isEmpty) {
        print('No hay tareas pendientes.');
        return;
    }

    print('Tareas pendientes:');
    for (int i = 0; i < tareasPendientes.length; i++) {
        print('${i + 1}. ${tareasPendientes[i]}');
    }

    stdout.write('Ingrese el número de la tarea completada: ');
    int indice = int.parse(stdin.readLineSync()!) - 1;

    if (indice >= 0 && indice < tareasPendientes.length) {
        String tareaCompletada = tareasPendientes.removeAt(indice);
        tareasCompletadas.add(tareaCompletada);
        print('Tarea marcada como completada.');
    } else {
        print('Índice inválido.');
    }
}

void eliminarTarea() {
    if (tareasPendientes.isEmpty) {
        print('No hay tareas pendientes.');
        return;
    }

    print('Tareas pendientes:');
    for (int i = 0; i < tareasPendientes.length; i++) {
        print('${i + 1}. ${tareasPendientes[i]}');
    }

    stdout.write('Ingrese el número de la tarea a eliminar: ');
    int indice = int.parse(stdin.readLineSync()!) - 1;

    if (indice >= 0 && indice < tareasPendientes.length) {
        tareasPendientes.removeAt(indice);
        print('Tarea eliminada correctamente.');
    } else {
        print('Índice inválido.');
    }
}

void verTareasPendientes() {
    if (tareasPendientes.isEmpty) {
        print('No hay tareas pendientes.');
    } else {
        print('Tareas pendientes:');
        for (int i = 0; i < tareasPendientes.length; i++) {
            print('${i + 1}. ${tareasPendientes[i]}');
        }
    }
}

void verTareasCompletadas() {
    if (tareasCompletadas.isEmpty) {
        print('No hay tareas completadas.');
    } else {
        print('Tareas completadas:');
        for (int i = 0; i < tareasCompletadas.length; i++) {
            print('${i + 1}. ${tareasCompletadas[i]}');
        }
    }
}

void main() {
    int opcion = 0;

    do {
        mostrarMenu();
        opcion = int.parse(stdin.readLineSync()!);

        switch (opcion) {
            case 1:
                agregarTarea();
                break;
            case 2:
                marcarTareaCompletada();
                break;
            case 3:
                eliminarTarea();
                break;
            case 4:
                verTareasPendientes();
                break;
            case 5:
                verTareasCompletadas();
                break;
            case 6:
                print('Programa finalizado');
                break;
            default:
                print('Opción inválida. Ingrese una opcion correcta');
                break;
        }
    } while (opcion != 6);
}