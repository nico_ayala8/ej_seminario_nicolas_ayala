import 'dart:math';
import 'dart:io';

String generarContrasena(int longitud) {
    const letrasMinusculas = 'abcdefghijklmnopqrstuvwxyz';
    const letrasMayusculas = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const numeros = '0123456789';
    const simbolos = '!@#\$%^&*()_+~`|=\\{}[]:;\"\'<>,.?/-';

    String caracteres = '$letrasMinusculas$letrasMayusculas$numeros$simbolos';
    // numeros aleatorios seguros.
    Random random = Random.secure();

    String contrasena = List.generate(longitud, (index) {
        int indiceAleatorio = random.nextInt(caracteres.length);
        return caracteres[indiceAleatorio];
    }).join();

    return contrasena;
}

void main() {
    print('Generador de Contraseñas Seguras');

    stdout.write('Ingrese la longitud de la contraseña: ');
    int longitud = int.parse(stdin.readLineSync()!);

    String contrasena = generarContrasena(longitud);
    print('La contraseña generada es: $contrasena');
}