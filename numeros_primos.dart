void main() {
    // Creamos una lista de booleanos 'esPrimo' de longitud 21 (para cubrir desde 0 hasta 20)
    // e inicialmente marcamos todos los elementos como true
    List<bool> esPrimo = List.filled(21, true);

    // Inicializamos una lista vacía 'primos' para almacenar los números primos
    List<int> primos = [];

    // Marcamos 0 y 1 como no primos, ya que son casos especiales
    esPrimo[0] = esPrimo[1] = false;

    // Aplicamos el algoritmo de la criba de Eratóstenes
    // Iteramos desde 2 hasta la raíz cuadrada de 20
    for (int i = 2; i * i <= 20; i++) {
        // Si 'i' es un número primo (esPrimo[i] es true)
        if (esPrimo[i]) {
            // Marcamos todos los múltiplos de 'i' como no primos
            for (int j = i * i; j <= 20; j += i) {
                esPrimo[j] = false;
            }
        }
    }

    // Después de aplicar la criba, recorremos la lista 'esPrimo'
    for (int i = 2; i <= 20; i++) {
        // Si esPrimo[i] es true, significa que 'i' es un número primo
        if (esPrimo[i]) {
            // Agregamos 'i' a la lista de primos
            primos.add(i);
        }
    }

    // Imprimimos la lista de números primos
    print("Los números primos hasta 20 son: $primos");
}