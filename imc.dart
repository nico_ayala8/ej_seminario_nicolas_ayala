import 'dart:io';

double calcularIMC(double peso, double altura) {
    // Calculamos el IMC
    double imc = peso / (altura * altura);
    return imc;
}

void main() {
  // Pedimos al usuario que ingrese su peso y altura
    stdout.write('Ingrese su peso en kilogramos: ');
    double peso = double.parse(stdin.readLineSync()!);

    stdout.write('Ingrese su altura en metros: ');
    double altura = double.parse(stdin.readLineSync()!);

  // Calculamos el IMC
    double imc = calcularIMC(peso, altura);

  // Mostramos el resultado
    print('Su Índice de Masa Corporal (IMC) es: ${imc.toStringAsFixed(2)}');
}